jQuery(document).ready(function($) {
    $('fieldset.collapsed legend').on('click', function () {
        $(this).parent().find('div.fieldset_container').toggle();
    });

    $('form a.add_items').on('click', function (e) {
        e.preventDefault();

        $('.add_dialog#additional_pairings').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: '30%',
            title: $('.add_dialog#additional_pairings h4').text(),
            dialogClass: 'add_items_dialog',
            open: function () {
                $('.ui-widget-overlay').addClass('custom-overlay');
            },
            close: function () {
                $(this).dialog('destroy');
            },
            buttons: {
                Ok: function() {
                    $.post(
                        'http://62.4.18.85/secret-santa/ajax',
                        {
                            submit: 'additional_pairings',
                            data: $('#additional_pairings textarea').val()
                        },
                        function(data) {
                            var result_messages = '';

                            for (i in data) {
                                result_messages+= '<p class="messages ' + data[i].status + '">' + data[i].message + '</p>';
                            }

                            $('ul.hyphenated li').animate(
                                {padding: 0, height: 0},
                                400,
                                function () { $(this).hide(); }
                            );
                            $('textarea').animate(
                                {padding: 0, height: 0},
                                400,
                                function () { $(this).hide(); }
                            );

                            $('.add_items_dialog .ui-dialog-buttonpane').hide();
                            $('.add_items_dialog .add_dialog').prepend(result_messages);
                        }, 'json');
                },
                Cancel: function() {
                    $(this).dialog('destroy');
                }
            }
        });
    });
});
