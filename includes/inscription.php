<div class="form-wrapper whole-page">
    <h2>Inscription form</h2>
    <p class="intro">The personnal information you're providing here are to remain strictly confidential. They will be used as part of the event to communicate with you and match you with your secret santa buddy. You can read more by <a href="<?= BASE_PATH ?>/privacy">clicking here</a>.</p>

    <?
    if ($messages = get_flash_messages()):
        foreach ($messages as $message): ?>
            <p class="messages <?= $message['level'] ?>"><?= $message['message'] ?></p>
        <? endforeach;
    endif;
    ?>

    <form action="#" method="POST">
        <p>
            <label>Your nick</label>
            <input type="text" name="nick" value="<?= ($nick = $_SESSION['inscription_values']['nick']) ? $nick: '' ?>" maxlength="200" required="required"<? if ($nick = $_SESSION['inscription_values']['nick']): echo $nick; endif; ?>>
        </p>
        <p>
            <label>Are you over 18?</label>

            <label for="of_age">Yes</label>
            <input type="radio" name="is_of_age" id="of_age" value="1">

            <label for="underage">No</label>
            <input type="radio" name="is_of_age" id="underage" value="0">
        </p>

        <fieldset>
            <legend>Ways to contact you</legend>
            <span class="small">We won't spam, we promise</span>

            <p>
                <label>Email</label>
                <input type="email" name="email" value="<?= ($email = $_SESSION['inscription_values']['email']) ? $email: '' ?>" required="required">
            </p>
            <p>
                <label>Tumblr</label>
                <input type="text" name="tumblr" value="<?= ($tumblr = $_SESSION['inscription_values']['tumblr']) ? $tumblr: '' ?>">
            </p>
            <p>
                <label>Twitter</label>
                <input type="text" name="twitter" value="<?= ($twitter = $_SESSION['inscription_values']['twitter']) ? $twitter: '' ?>">
            </p>
        </fieldset>

        <fieldset>
            <legend>Your WIP</legend>
            <span class="small">What's it like?</span>

            <p>
                <label for="wip_file">
                    Your file
                    <span>
                        Max. 10Mo or 10,000 words<br>
                        Accepted formats: .pdf, .docx, .txt
                    </span>
                </label>
                <input type="file" name="wip_file" id="wip_file" file=".docx, .pdf, .txt">
            </p>

            <p>
                <label>Rating</label>

                <select name="sender_rating" class="select2" required="required">
                    <option value=""></option>
                    <option value="general"<?= preselected('sender_rating', 'general') ?>>General</option>
                    <option value="teenagers"<?= preselected('sender_rating', 'teenagers') ?>>Teenagers</option>
                    <option value="mature"<?= preselected('sender_rating', 'mature') ?>>Mature</option>
                    <option value="explicit"<?= preselected('sender_rating', 'explicit') ?>>Explicit</option>
                </select>
            </p>

            <p>
                <label>
                    Your WIP's pairings
                    <span>
                        <b>ctrl+left click</b> to select more than one<br>
                        Can't find yours? <a href="" class="add_items">Add it!</a>
                    </span>
                </label>

                <select name="sender_pairings[]" class="select2" multiple>
                    <option value="">No</option>

                    <? if ($all_pairings):
                        foreach ($all_pairings as $pairing): ?>
                            <option value="<?= $pairing['id'] ?>"<?= preselected('sender_pairings', $pairing['id']) ?>>
                                <?= $pairing['name'] ?>
                            </option>
                        <? endforeach;
                    endif; ?>
                </select>
            </p>

            <p>
                <label>
                    Your WIP's AUs
                    <span>
                        <b>ctrl+left click</b> to select more than one<br>
                        Hover over items to show the whole content
                    </span>
                </label>

                <select name="sender_aus[]" class="select2" multiple>
                    <option value="">No</option>

                    <? if ($all_aus):
                        foreach ($all_aus as $au): ?>
                            <option value="<?= $au['id'] ?>" title="<?= $au['name'] ?>"<?= preselected('sender_aus', $au['id']) ?>>
                                <?= $au['name'] ?>
                            </option>
                        <? endforeach;
                    endif; ?>
                </select>

            </p>
        </fieldset>

        <fieldset>
            <legend>Your buddy's WIP</legend>
            <span class="small">What would you write?</span>

            <p>
                <label>Rating</label>

                <select name="receiver_rating" class="select2" required="required">
                    <option value=""></option>
                    <option value="general"<?= preselected('receiver_rating', 'general') ?>>General only</option>
                    <option value="teenagers"<?= preselected('receiver_rating', 'teenagers') ?>>Teenagers or lower</option>
                    <option value="mature"<?= preselected('receiver_rating', 'mature') ?>>Mature or lower</option>
                    <option value="explicit"<?= preselected('receiver_rating', 'explicit') ?>>Explicit or lower</option>
                </select>
            </p>

            <p>
                <label>
                    Pairing(s) you <b>don't</b> want
                    <span><b>ctrl+left click</b> to select more than one</span>
                </label>

                <select name="receiver_pairings[]" class="select2" multiple>
                    <option value="">Surprise me!</option>
                    <option value="no_pairing">No pairing</option>

                    <? if ($all_pairings):
                        foreach ($all_pairings as $pairing): ?>
                            <option value="<?= $pairing['id'] ?>"<?= preselected('receiver_pairings', $pairing['id']) ?>>
                                <?= $pairing['name'] ?>
                            </option>
                        <? endforeach;
                    endif; ?>
                </select>
            </p>

            <p>
                <label>
                    AUs you <b>don't</b> want
                    <span>
                        <b>ctrl+left click</b> to select more than one<br>
                        Hover over items to show the whole content
                    </span>
                </label>

                <select name="receiver_aus[]" class="select2" multiple>
                    <option value="">Surprise me!</option>
                    <option value="no_au">No AU</option>

                    <? if ($all_aus):
                        foreach ($all_aus as $au): ?>
                            <option value="<?= $au['id'] ?>" title="<?= $au['name'] ?>"<?= preselected('receiver_aus', $au['id']) ?>>
                                <?= $au['name'] ?>
                            </option>
                        <? endforeach;
                    endif; ?>
                </select>
            </p>
        </fieldset>

        <? unset($_SESSION['inscription_values']); ?>

        <p><input class="button submit" type="submit" name="submit" value="Submit"></p>
    </form>

    <div id="additional_pairings" class="add_dialog">
        <h4>Add pairings</h4>

        <ul class="hyphenated">
            <li>separate pairings by commas</li>
            <li>only letters, spaces and /</li>
            <li>you won't lose what you've written so far</li>
        </ul>
        <textarea name="additional_pairings" rows="5" cols="40"></textarea>
    </div>
</div>
