<!DOCTYPE html>
<html>
    <head>
        <title>Hi!</title>
        <meta name="charset" content="utf-8">
        <link href="https://fonts.googleapis.com/css?family=Nunito|Niramit" rel="stylesheet">
        <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" rel="text/javascript"></script> -->

        <link rel="stylesheet" type="text/css" href="<?= BASE_PATH ?>/public/css/styles.css">
        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    </head>
    <body>
        <a id="githubg_link" title="Project sources" target="_blank" href="https://framagit.org/Liezel/secret-santa/">&nbsp;</a>
        <header class="container">
            <h1><a href="<?= BASE_PATH ?>">Welcome!</a></h1>
        </header>

