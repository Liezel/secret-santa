        <footer class="container">
            <a href="<?= BASE_PATH ?>/privacy">Privacy policy</a>
        </footer>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" rel="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" rel="text/javascript"></script>

        <script src="<?= BASE_PATH ?>/public/js/scripts.js" rel="text/javascript"></script>
    </body>
</html>
