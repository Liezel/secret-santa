<h2>Privacy Policy</h2>

<h3>Your data</h3>

<p>We collect the data you provide to us by means of the <a href="<?= BASE_PATH ?>/inscription">registration form</a> and in the sole purpose of matching you with a secret santa buddy. These informations are:</p>

<ul class="hyphenated">
    <li>your nickname;</li>
    <li>your email address;</li>
    <li>your age.</li>
</ul>

<p>They will only be used during this event. No one except the mods and the developper of this app will ever have access to them.</p>

<p>If you want your data deleted, you simply have to send an email at this address: secret[dot]santa[dot]wip[dot]event[at]gmail[dot]com.</p>

<h3>WIP</h3>

<p>The WIP or outline you submit will be sent to your buddy and won't be disclosed to anyone else. Mods won't have access to it and the developper of the app won't peak. It will be deleted for good from the server at the end of the event, or when you opt out if you do.</p>

<h3>Emails</h3>

<p>You will receive an email from us if you register to the Secret Santa Writing Event that takes place in november and december 2018. This email will provide you with a unique link that will enable you to participate to the event. Mods may send you an email in regards to your participation to this event using their own email accounts.</p>

<h3>Cookies</h3>

<p>No persistent cookies are used on this website. We use temporary session cookies that are deleted automatically by your web browser when you close it.</p>

<h3>Logs</h3>

We don't log your activity on this website. We do log errors if they happen, at which point we log your IP address.

<h3>Hosting</h3>

<p>The present server is hosted by the French service <a target="_blank" href="https://www.omgserv.com/fr/">OMG Serv</a>.</p>
