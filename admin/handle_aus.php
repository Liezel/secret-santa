<?php
$errors = [];
$au_to_edit = $all_aus = null;

if ($_GET['action'] == 'aus') {
    if (!empty($_POST['submit'])) {
        $success = false;
        $to_check = [
            ['name' => 'AU name', 'type' => 'text',    'value' => $_POST['au_name']]
        ];

        $message = 'AU was successfully added.';

        if (!empty($_POST['id'])) {
            $to_check[] = ['name' => 'ID', 'type' => 'number', 'value' => $_POST['id']];
            $message = 'AU was successfully updated.';
        }

        $errors = check_inputs($to_check);

        if (empty($errors)) {
            $errors = add_au($_POST);
        }

        flash_results($errors, $message);

        header('Location: ' . BASE_PATH . '/admin/aus');
    } else {
        $all_aus = get_all_aus();
    }
} elseif (in_array($_GET['action'], ['edit_au', 'delete_au'])) {
    if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
        if ($_GET['action'] == 'edit_au') {
            $sql_edit = 'SELECT * FROM `aus` WHERE `id` = :id';
            $query_edit = $dbh->prepare($sql_edit);
            $res_edit = $query_edit->execute(['id' => $_GET['id']]);

            if ($res_edit) {
                $au_to_edit = $query_edit->fetch();
            }

            $all_aus = get_all_aus();
        } else {
            $message = '';
            $errors = check_inputs([['name' => 'ID', 'type' => 'number', 'value' => $_GET['id']]]);

            if (empty($errors)) {
                $sql_delete = 'DELETE FROM `aus` WHERE `id` = :id';
                $query_delete = $dbh->prepare($sql_delete);
                $res_delete = $query_delete->execute(['id' => $_GET['id']]);

                if ($res_delete) {
                    $message = 'The AU was successfully deleted.';
                } else {
                    $errors = [['An unexpected error occurred.']];
                }
            }

            flash_results($errors, $message);

            header('Location: ' . BASE_PATH . '/admin/aus');
        }
    }

} else {
    header('Location: ' . BASE_PATH . '/admin/aus');
}

function get_all_aus()
{
    global $dbh;
    $all_aus = null;

    $sql_all = 'SELECT * FROM aus ORDER BY name';
    $query_all = $dbh->prepare($sql_all);
    $res_all = $query_all->execute();

    if ($res_all) {
        $all_aus = $query_all->fetchAll();
    }

    return $all_aus;
}
