<div class="content admin_content all_pairings">
    <h2>Manage pairings</h2>

    <? if (!isset($pairing_to_edit)): ?>
        <div class="form-wrapper">
            <form action="#" method="post" accept-charset="utf-8">
                <fieldset class="collapsed">
                    <legend>Add a pairing</legend>

                    <div class="fieldset_container">
                        <p><input type="text" name="pairing_name" value="" placeholder="Name"></p>
                        <p><input type="submit" name="submit" class="button submit" value="Submit"></p>
                    </div>
                </fieldset>
            </form>
        </div>
    <? endif; ?>

    <?
    if ($all_pairings): ?>
        <ul>
            <? foreach ($all_pairings as $pairing): ?>
                <li>
                    <? if (!isset($pairing_to_edit) || $pairing_to_edit['id'] != $pairing['id']) : ?>
                        <span class="first col"><?= $pairing['name']; ?></span>

                        <span class="second col">
                            <a class="action button edit" href="<?= BASE_PATH ?>/admin/edit_pairing/<?= $pairing['id'] ?>">Edit</a>
                            <a class="action button delete" href="<?= BASE_PATH ?>/admin/delete_pairing/<?= $pairing['id'] ?>">Delete</a>
                        </span>
                    <? elseif ($pairing_to_edit['id'] == $pairing['id']): ?>
                        <form action="<?= BASE_PATH ?>/admin/pairings" method="post" class="inline_form" accept-charset="utf-8">
                            <span class="first col">
                                <input type="text" name="pairing_name" value="<?= $pairing['name'] ?>" maxlength="255" size="55" placeholder="Name">
                            </span>
                            <span class="second col">
                                <input type="submit" name="submit" class="action button submit" value="Submit">
                            </span>
                            <input type="hidden" name="id" value="<?= $pairing['id'] ?>">
                        </form>
                    <? endif; ?>
                </li>
            <? endforeach; ?>
        </ul>
    <? endif; ?>
</div>
