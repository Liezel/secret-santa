<?php
$errors = [];
$pairing_to_edit = $all_pairings = null;

if ($_GET['action'] == 'pairings') {
    if (!empty($_POST['submit'])) {
        $success = false;
        $to_check = [
            ['name' => 'Pairing name', 'type' => 'text',        'value' => $_POST['pairing_name']],
        ];

        $message = 'Pairing was successfully added.';

        if (!empty($_POST['id'])) {
            $to_check[] = ['name' => 'ID', 'type' => 'number', 'value' => $_POST['id']];
            $message = 'Pairing was successfully updated.';
        }

        $errors = check_inputs($to_check);

        if (empty($errors)) {
            $errors = add_pairing($_POST);
        }

        flash_results($errors, $message);

        header('Location: ' . BASE_PATH . '/admin/pairings');
    } else {
        $all_pairings = get_all_pairings();
    }
} elseif (in_array($_GET['action'], ['edit_pairing', 'delete_pairing'])) {
    if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
        if ($_GET['action'] == 'edit_pairing') {
            $sql_edit = 'SELECT * FROM `pairings` WHERE `id` = :id';
            $query_edit = $dbh->prepare($sql_edit);
            $res_edit = $query_edit->execute(['id' => $_GET['id']]);

            if ($res_edit) {
                $pairing_to_edit = $query_edit->fetch();
            }

            $all_pairings = get_all_pairings();
        } else {
            $message = '';
            $errors = check_inputs([['name' => 'ID', 'type' => 'number', 'value' => $_GET['id']]]);

            if (empty($errors)) {
                $sql_delete = 'DELETE FROM `pairings` WHERE `id` = :id';
                $query_delete = $dbh->prepare($sql_delete);
                $res_delete = $query_delete->execute(['id' => $_GET['id']]);

                if ($res_delete) {
                    $message = 'The pairing was successfully deleted.';
                } else {
                    $errors = [['An unexpected error occurred.']];
                }
            }

            flash_results($errors, $message);

            header('Location: ' . BASE_PATH . '/admin/pairings');
        }
    }

} else {
    header('Location: ' . BASE_PATH . '/admin/pairings');
}

function get_all_pairings()
{
    global $dbh;
    $all_pairings = null;

    $sql_all = 'SELECT * FROM pairings ORDER BY name';
    $query_all = $dbh->prepare($sql_all);
    $res_all = $query_all->execute();

    if ($res_all) {
        $all_pairings = $query_all->fetchAll();
    }

    return $all_pairings;
}
