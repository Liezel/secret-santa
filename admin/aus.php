<div class="content admin_content all_aus">
    <h2>Manage AUs</h2>

    <div class="form-wrapper">
        <? if (!isset($au_to_edit)): ?>
            <form action="#" method="post" accept-charset="utf-8">
                <fieldset class="collapsed">
                    <legend>Add an AU</legend>

                    <div class="fieldset_container">
                        <p><input type="text" name="au_name" value="" placeholder="Name"></p>
                        <p>
                            <label for="is_sfw" class="small_label">Is AU SFW?</label>
                            <input type="checkbox" name="is_sfw" id="is_sfw" value="1">
                        </p>
                        <p><input type="submit" name="submit" class="button submit" value="Submit"></p>
                    </div>
                </fieldset>
            </form>
        <? endif; ?>
    </div>

    <?
    if ($all_aus): ?>
        <ul>
            <? foreach ($all_aus as $au) { ?>
                <li>
                    <? if (!isset($au_to_edit) || $au_to_edit['id'] != $au['id']) : ?>
                        <span class="first col">
                            <? if ($au['is_sfw']): ?>
                                <span class="is_sfw">[SFW]</span>
                            <? else: ?>
                                <span class="is_not_sfw">[NSFW]</span>
                            <? endif; ?>
                            &nbsp;
                            <?= $au['name']; ?>
                        </span>
                        <span class="second col">
                            <a class="action button edit" href="<?= BASE_PATH ?>/admin/edit_au/<?= $au['id'] ?>">Edit</a>
                            <a class="action button delete" href="<?= BASE_PATH ?>/admin/delete_au/<?= $au['id'] ?>">Delete</a>
                        </span>
                    <? elseif ($au_to_edit['id'] == $au['id']): ?>
                        <form action="<?= BASE_PATH ?>/admin/aus" method="post" class="inline_form" accept-charset="utf-8">
                            <span class="first col">
                                <input type="text" name="au_name" value="<?= $au['name'] ?>" maxlength="255" size="55" placeholder="Name">
                                <label for="is_sfw">Is AU SFW?</label>
                                <input type="checkbox" name="is_sfw" id="is_sfw" value="1"<? if ($au['is_sfw']): ?> checked="checked"<?php endif; ?>>
                            </span>
                            <span class="second col">
                                <input type="submit" name="submit" class="action button submit" value="Submit">
                            </span>
                            <input type="hidden" name="id" value="<?= $au['id'] ?>">
                        </form>
                    <? endif; ?>
                </li>
            <? } ?>
        </ul>
    <? endif; ?>
</div>
