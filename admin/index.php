<?php
require('../includes/parameters.php');

if (empty($_GET['action'])) {
    header('Location: ' . BASE_PATH . '/admin/pairings');
}

require('../src/core.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Administration</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito|Lato" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASE_PATH ?>/public/css/styles.css">
    </head>

    <body>
        <header class="container admin">
            <h1>Administration</h1>
        </header>
        <nav id="admin_nav">
            <ul>
                <li>
                    <a class="home" href="<?= BASE_PATH ?>" title="Home">&nbsp;</a>
                </li>
                <li>
                    <a href="<?= BASE_PATH ?>/admin/pairings" <? if (in_array($_GET['action'], ['pairings', 'edit_pairing'])): ?>class="active"<? endif; ?>>Pairings</a>
                </li>
                <li>
                    <a href="<?= BASE_PATH ?>/admin/aus" <? if (in_array($_GET['action'], ['aus', 'edit_au'])): ?>class="active"<? endif; ?>>AUs</a>
                </li>
                <li>
                    <a title="Soon">Matches</a>
                </li>
            </ul>
        </nav>

        <section id="wrapper" class="container">
            <?
            if ($messages = get_flash_messages()):
                foreach ($messages as $message): ?>
                    <p class="messages <?= $message['level'] ?>"><?= $message['message'] ?></p>
                <? endforeach;
            endif;

            switch($_GET['action']) {
                case 'pairings':
                case 'edit_pairing':
                case 'delete_pairing':
                    require('handle_pairings.php');
                    require('pairings.php');
                    break;

                case 'aus':
                case 'edit_au':
                case 'delete_au':
                    require('handle_aus.php');
                    require('aus.php');
                    break;

                default:
                    include('admin_homepage.php');
                    break;
            } ?>
        </section>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" rel="text/javascript"></script>
        <script src="<?= BASE_PATH ?>/public/js/scripts.js" rel="text/javascript"></script>
    </body>
</html>
