<?
require('includes/parameters.php');
require(APP_PATH . '/src/core.php');

$action = !empty($_GET['action']) ? $_GET['action'] : 'inscription';

if (!in_array($action, $routes) || (in_array($action, $constraints) && !isset($_SESSION[$action]))) {
    header('Location: ' . BASE_PATH);
}

if ($action !== 'ajax') {
    include(APP_PATH . '/includes/header.php');
} else {
    require(APP_PATH . '/src/ajax.php');
}
?>

<section id="wrapper" class="container">
    <?
    if ($messages = get_flash_messages()):
        foreach ($messages as $message): ?>
            <p class="messages <?= $message['level'] ?>"><?= $message['message'] ?></p>
        <? endforeach;
    endif;

    $view = APP_PATH . '/src/handler_' . $action . '.php';
    $handler = APP_PATH . '/includes/' . $action . '.php';

    if (file_exists($view)) {
        include($view);
    }
    if (file_exists($handler)) {
        include($handler);
    } ?>
</section>

<?
include(APP_PATH . '/includes/footer.php');
