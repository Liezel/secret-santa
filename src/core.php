<?php

session_start();

global $dbh;
$dbh = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME, USER, PASS, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

function dd()
{
    array_map(function($x) {
        echo '<pre>';
        var_dump($x);
    }, func_get_args());

    die;
}

function dump()
{
    array_map(function($x) {
        echo '<pre>';
        var_dump($x);
    }, func_get_args());
}

function dd_query($query, $args)
{
    $query_string = sprintf($query, $args);
    echo $query_string;
    die;
}

function set_flash_message($level, $message)
{
    $_SESSION['flash'][] = ['level' => $level, 'message' => $message];
}

function get_flash_messages()
{
    $flash = [];

    if (isset($_SESSION['flash'])) {
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
    }

    return $flash;
}

function flash_results($errors, $message = '')
{
    if (!empty($errors)) {
        foreach ($errors as $error) {
            set_flash_message('error', $error);
        }
    } elseif ($message !== '') {
        set_flash_message('success', $message);
    }
}

function check_inputs($inputs)
{
    $errors = [];

    if (!is_array($inputs)) {
        $errors['message'] = 'An unexpected error occurred, please contact one of the mods';
    } else {
        foreach ($inputs as $input) {
            if (is_null($input['value'])) {
                $errors = [$input['name'] . ' can\'t be empty'];
            } else {
                if (is_array($input['value'])) {
                    foreach ($input['value'] as $value) {
                        $errors = check_type($input['type'], $value, $input['name']);
                    }
                } else {
                    $errors = check_type($input['type'], $input['value'], $input['name']);
                }
            }
        }
    }

    return $errors;
}

function check_type($type, $value, $name)
{
    switch($type) {
        case 'text':
            if (!preg_match('/^[a-zA-Z]+[\-\/\w\s]*$/', $value)) {
                $errors[] = 'Error on ' . $name . ': accepted characters are letters, spaces, slashes and hyphens';
            }
            break;

        case 'plain_text':
            if (!preg_match('/^[a-zA-Z]+$/', trim($value))) {
                $errors[] = 'Error on ' . $name . ' : only letters are allowed';
            }
            break;

        case 'number':
            if (!is_numeric($value)) {
                $errors[] = 'Error on ' . $name . ' : an unexpected error occurred, please contact one of the mods';
            }
            break;

        case 'email':
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $errors[] = 'Error on ' . $name . ' : your email address is invalid';
            }
            break;

        default:
            $errors[] = 'An unexpected error occurred, please contact one of the mods';
            break;
    }
}

function add_pairing($data)
{
    global $dbh;
    $sql_check = 'SELECT id FROM pairings WHERE name = :pairing_name';
    $query_check = $dbh->prepare($sql_check);
    $query_check->execute(['pairing_name' => $data['pairing_name']]);
    $res_check = $query_check->fetch();

    if (!$res_check) {
        $args = ['name' => trim($data['pairing_name'])];

        if (!isset($data['id'])) {
            $sql = 'INSERT INTO `pairings` (`name`) VALUES (:name)';
        } else {
            $sql = 'UPDATE `pairings` SET `name` = :name WHERE `id` = :id';
            $args['id'] = $data['id'];
        }

        $errors = query_stuff($sql, $args);
    } else {
        $errors = [[$data['pairing_name'] . ': this pairing already exists!']];
    }

    return $errors;
}

function add_au($data)
{
    global $dbh;
    $sql_check = 'SELECT id FROM aus WHERE name = :au_name';
    $query_check = $dbh->prepare($sql_check);
    $query_check->execute(['au_name' => $data['au_name']]);
    $res_check = $query_check->fetch();

    if (!$res_check) {
        $is_sfw = !empty($data['is_sfw']) ? '1' : '0';
        $args = ['name' => trim($data['au_name']), 'is_sfw' => $is_sfw];

        if (!isset($data['id'])) {
            $sql = 'INSERT INTO `aus` (`name`, `is_sfw`) VALUES (:name, :is_sfw)';
        } else {
            $sql = 'UPDATE `aus` SET `name` = :name, `is_sfw` = :is_sfw WHERE `id` = :id';
            $args['id'] = $data['id'];
        }

        $errors = query_stuff($sql, $args);
    } else {
        $errors = [[$data['au_name'] . ': this AU already exists!']];
    }

    return $errors;
}

function query_stuff($sql, $args)
{
    global $dbh;
    $errors = [];
    $query = $dbh->prepare($sql);

    try {
        $query->execute($args);
    } catch(Exception $e) {
        $errors = [['An unexpected error occurred, please contact one of the mods with the following message: ' . $e->getMessage()]];
    }

    return $errors;
}

function preselected($key, $value)
{
    if (is_array($_SESSION['inscription_values'][$key])) {
        return (in_array($value, $_SESSION['inscription_values'][$key])) ? ' selected="selected"': '';
    } else {
        return ($value == $_SESSION['inscription_values'][$key]) ? ' selected="selected"': '';
    }
}
