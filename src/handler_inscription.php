<?php

if (!empty($_POST['submit'])) {
    $errors = [];

    // Add the required fields
    $to_check = [
        ['name' => 'Nick',           'type' => 'text',             'value' => $_POST['nick']],
        ['name' => 'Age',            'type' => 'number',           'value' => $_POST['is_of_age']],
        ['name' => 'Email',          'type' => 'email',            'value' => $_POST['email']],
        ['name' => 'WIP\'s rating',  'type' => 'plain_text',       'value' => $_POST['sender_rating']],
        ['name' => 'Gift\'s rating', 'type' => 'plain_text',       'value' => $_POST['receiver_rating']],
    ];

    // Add the optional ones
    if (!empty($_POST['tumblr'])) {
        $to_check[] = ['name' => 'Tumblr',  'type' => 'text', 'value' => $_POST['tumblr']];
    }
    if (!empty($_POST['twitter'])) {
        $to_check[] = ['name' => 'Twitter', 'type' => 'text', 'value' => $_POST['twitter']];
    }

    $relations = [
        'receiver_pairings' => ['name' => 'Sender\'s pairing', 'table' => 'pairings', 'singular' => 'Pairing', 'id_name' => 'pairing_id', 'type' => 'gift'],
        'receiver_aus'      => ['name' => 'WIP AU',            'table' => 'aus',      'singular' => 'AU',      'id_name' => 'au_id',      'type' => 'wip'],
        'sender_pairings'   => ['name' => 'WIP\'s pairing',    'table' => 'pairings', 'singular' => 'Pairing', 'id_name' => 'pairing_id', 'type' => 'wip'],
        'sender_aus'        => ['name' => 'Gift\'s AU',        'table' => 'aus',      'singular' => 'AU',      'id_name' => 'au_id',      'type' => 'gift'],
    ];

    foreach ($relations as $key => $relation) {
        if (!empty($_POST[$key])) {
            if (('receiver_pairings' === $key && 'no_pairing' === $_POST[$key]) ||
                ('receiver_aus' === $key && 'no_au' === $_POST[$key])) {
                unset($relations[$key]);
                continue;
            }

            $to_check[] = [
                'name' => $relation['name'],
                'type' => 'number',
                'value' => $_POST[$key]
            ];
        } else {
            unset($relations[$key]);
        }
    }

    $errors = check_inputs($to_check);

    $sql_num_reg = 'SELECT id as c FROM users WHERE email = ?';
    $query_num_reg = $dbh->prepare($sql_num_reg);
    $res_num_reg = $query_num_reg->execute([trim($_POST['email'])]);

    if ($res_num_reg) {
        $count = $query_num_reg->rowCount();

        if (MAX_REGISTRATIONS < $count) {
            $errors[] = 'You\'ve reached the maximum number of ' . MAX_REGISTRATIONS . ' registrations per person!';
        }
    }

    $is_of_age = intval($_POST['is_of_age']);

    if ($is_of_age === 0 && !in_array($receiver_rating, ['general', 'teenagers'])) {
        $errors[] = 'You can only apply for Mature or Explicit works if you are over 18';
    }

    if (empty($errors)) {
        $nick            = trim($_POST['nick']);
        $email           = trim($_POST['email']);
        $tumblr          = trim($_POST['tumblr']);
        $twitter         = trim($_POST['twitter']);
        $sender_rating   = trim($_POST['sender_rating']);
        $receiver_rating = trim($_POST['receiver_rating']);

        $relation_arg = [];

        $dbh->beginTransaction();

        try {
            $sql_user = 'INSERT INTO users (username, email, is_of_age, tumblr, twitter, sender_rating, receiver_rating, accepts_no_pairing, accepts_no_au) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';

            $no_pairing = (isset($_POST['receiver_pairings']) && $_POST['receiver_pairings'] === 'no_pairing') ? 1 : 0;
            $no_au      = (isset($_POST['receiver_aus']) && $_POST['receiver_aus'] === 'no_au') ? 1 : 0;
            $args_user  = [$nick, $email, $is_of_age, $tumblr, $twitter, $sender_rating, $receiver_rating, $no_pairing, $no_au];
            $query_user = $dbh->prepare($sql_user);
            $query_user->execute($args_user);
            $user_id = $dbh->lastInsertId();

            if (!empty($user_id) && !empty($relations)) {
                foreach ($relations as $key => $relation) {
                    foreach ($_POST[$key] as $rel_id) {
                        if ($error_rel = handle_relation($key, $relation, $user_id, $rel_id)) {
                            $errors[] = $error_rel;
                            break 2;
                        }
                    }
                }
            } elseif (empty($user_id)) {
                $errors[] = 'An unexpected error occurred, please contact one of the mods with the following code : EMPTY_USER_ID.';
            }

            if (empty($errors)) {
                $_SESSION['inscription_successful'] = true;
                $dbh->commit();
                header('Location: ' . BASE_PATH . '/inscription_successful');
            }
        } catch (Exception $e) {
            $errors = [['An unexpected error occurred, please contact one of the mods with the following message: ' . $e->getMessage()]];
            $dbh->rollBack();
        }
    }

    if (!empty($errors)) {
        flash_results($errors);
        $_SESSION['inscription_values'] = $_POST;
    }

    header('Location: ' . BASE_PATH . '/inscription');
}

function handle_relation($key, $relation, $user_id, $value = null)
{
    global $dbh;

    if (is_null($value) && !empty($_POST[$key])) {
        $value = $_POST[$key];
    }

    if (is_null($value)) {
        return 'An unexpected error occurred, please contact one of the mods';
    }

    $sql_check = 'SELECT * FROM ' . $relation['table'] . ' WHERE id = ?';
    $query_check = $dbh->prepare($sql_check);
    $res_check = $query_check->execute([$value]);

    if (!$res_check) {
        return 'not_found';
    } else {
        if ($_POST['is_of_age'] === '0' && in_array($key, ['receiver_aus', 'sender_aus'])) {
            $au = $query_check->fetch();

            if ($au['is_sfw']) {
                return 'You can\'t select NSFW AUs if you\'re under 18';
            }
        }

        $sql_relation = 'INSERT INTO users_' . $relation['table'] . ' (`user_id`, `' . $relation['id_name'] . '`, `type`) VALUES (?, ?, ?)';
        $args_relation = [$user_id, $value, $relation['type']];
        $query_relation = $dbh->prepare($sql_relation);
        $res_relation = $query_relation->execute($args_relation);

        if (!$res_relation) {
            return 'An unexpected error occurred, please contact one of the mods';
        }
    }
    dump('yeah');
}

$sql_all_pairings = 'SELECT * FROM pairings ORDER BY name';
$query_all_pairings = $dbh->prepare($sql_all_pairings);
$res_all_pairings = $query_all_pairings->execute();

if ($res_all_pairings) {
    $all_pairings = $query_all_pairings->fetchAll();
}

$sql_all_aus = 'SELECT * FROM aus ORDER BY name';
$query_all_aus = $dbh->prepare($sql_all_aus);
$res_all_aus = $query_all_aus->execute();

if ($res_all_aus) {
    $all_aus = $query_all_aus->fetchAll();
}
