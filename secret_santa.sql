-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 11 Octobre 2018 à 14:12
-- Version du serveur :  5.7.23-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `secret_santa`
--

-- --------------------------------------------------------

--
-- Structure de la table `aus`
--

CREATE TABLE `aus` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pairings`
--

CREATE TABLE `pairings` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `age` tinyint(4) NOT NULL,
  `tumblr` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `sender_rating` varchar(20) NOT NULL,
  `receiver_rating` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users_aus`
--

CREATE TABLE `users_aus` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `au_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users_pairings`
--

CREATE TABLE `users_pairings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `aus`
--
ALTER TABLE `aus`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pairings`
--
ALTER TABLE `pairings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users_aus`
--
ALTER TABLE `users_aus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_aus_foreign_user` (`user_id`),
  ADD KEY `users_aus_foreign_au` (`au_id`);

--
-- Index pour la table `users_pairings`
--
ALTER TABLE `users_pairings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_pairings_foreign_user` (`user_id`),
  ADD KEY `users_pairings_foreign_pairing` (`pairing_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `aus`
--
ALTER TABLE `aus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pairings`
--
ALTER TABLE `pairings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users_aus`
--
ALTER TABLE `users_aus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users_pairings`
--
ALTER TABLE `users_pairings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `users_aus`
--
ALTER TABLE `users_aus`
  ADD CONSTRAINT `users_aus_foreign_au` FOREIGN KEY (`au_id`) REFERENCES `aus` (`id`),
  ADD CONSTRAINT `users_aus_foreign_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `users_pairings`
--
ALTER TABLE `users_pairings`
  ADD CONSTRAINT `users_pairings_foreign_pairing` FOREIGN KEY (`pairing_id`) REFERENCES `pairings` (`id`),
  ADD CONSTRAINT `users_pairings_foreign_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
