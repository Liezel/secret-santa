// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('public/css/*.scss')
        .pipe(sass({sourceComments: 'map', outputStyle: 'compressed'}))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/css'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('public/css/*.scss', gulp.series(['sass']));
});

// Default Task
gulp.task('default', gulp.series(['sass', 'watch']));
